.PHONY: lint build clean run deploy

lint:
	golint -set_exit_status

build:
	env GOOS=linux go build -ldflags="-s -w" -o bin/api api/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/cron cron/main.go

deploy: build
	./deploy.sh

clean:
	rm -rf ./bin
