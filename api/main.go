package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/aws/external"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/expression"
	"net/http"
	"os"
	"time"
)

type Response events.APIGatewayProxyResponse

type ResponseBody struct {
	Stats map[string]ResponseItem `json:"stats"`
}

type ResponseItem struct {
	City      string    `json:"city"`
	WindSpeed float64   `json:"windSpeed"`
	WindGust  float64   `json:"windGust"`
	Lat       float64   `json:"lat"`
	Lng       float64   `json:"lng"`
	UpdatedAt time.Time `json:"updatedAt"`
}

func Handler(ctx context.Context) (Response, error) {
	results, err := getResults(ctx, -7)
	if err != nil {
		return handleResponse(ResponseBody{}, http.StatusInternalServerError, err)
	}

	// return empty results
	if len(results.Items) == 0 {
		return handleResponse(ResponseBody{}, http.StatusOK, nil)
	}

	responseBody, err := prepareBody(results)
	if err != nil {
		return handleResponse(ResponseBody{}, http.StatusInternalServerError, err)
	}

	return handleResponse(responseBody, http.StatusOK, nil)
}

func getResults(ctx context.Context, days int) (*dynamodb.ScanResponse, error) {
	awsConfig, err := external.LoadDefaultAWSConfig()
	if err != nil {
		return nil, fmt.Errorf("AWS config failed to load: %v", err)
	}

	db := dynamodb.New(awsConfig)

	filter := expression.Name("Time").GreaterThanEqual(expression.Value(time.Now().AddDate(0, 0, days).Unix()))
	expr, err := expression.NewBuilder().WithFilter(filter).Build()
	if err != nil {
		return nil, fmt.Errorf("not able to build dynamodb expression: %v", err)
	}

	params := &dynamodb.ScanInput{
		TableName:                 aws.String(os.Getenv("TABLE_NAME")),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
	}
	sr := db.ScanRequest(params)

	results, err := sr.Send(ctx)
	if err != nil {
		return nil, fmt.Errorf("cannot fetch results: %v", err)
	}

	return results, nil
}

func valueToItem(resultItem map[string]dynamodb.AttributeValue) (ResponseItem, error) {
	type DbItem struct {
		ResponseItem
		Id   string `json:"id"`
		Time int64  `json:"time"`
	}

	dbItem := DbItem{}

	err := dynamodbattribute.UnmarshalMap(resultItem, &dbItem)
	if err != nil {
		return ResponseItem{}, fmt.Errorf("failed to unmarshal Record: %v", err)
	}

	item := ResponseItem{}
	item.City = dbItem.City
	item.WindSpeed = dbItem.WindSpeed
	item.WindGust = dbItem.WindGust
	item.Lat = dbItem.Lat
	item.Lng = dbItem.Lng
	item.UpdatedAt = time.Unix(dbItem.Time, 0)

	return item, nil
}

func prepareBody(results *dynamodb.ScanResponse) (ResponseBody, error) {
	firstItem, err := valueToItem(results.Items[0])
	if err != nil {
		return ResponseBody{}, err
	}

	body := ResponseBody{}
	body.Stats = map[string]ResponseItem{}
	body.Stats["min_wind_speed"] = firstItem
	body.Stats["max_wind_speed"] = firstItem

	for _, resultItem := range results.Items {
		item, err := valueToItem(resultItem)
		if err != nil {
			return ResponseBody{}, err
		}

		if item.WindSpeed < body.Stats["min_wind_speed"].WindSpeed {
			body.Stats["min_wind_speed"] = item
		}

		if item.WindSpeed > body.Stats["max_wind_speed"].WindSpeed {
			body.Stats["max_wind_speed"] = item
		}
	}

	return body, nil
}

func handleResponse(responseBody ResponseBody, statusCode int, err error) (Response, error) {
	if err != nil {
		return Response{StatusCode: statusCode}, err
	}

	body, err := json.Marshal(responseBody)
	if err != nil {
		return Response{}, err
	}

	resp := Response{
		StatusCode: statusCode,
		Body:       string(body),
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
	}

	return resp, nil
}

func main() {
	lambda.Start(Handler)
}
