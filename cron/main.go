package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/aws/external"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/dynamodbattribute"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"
)

type ResponseData struct {
	RoutingVersion int       `json:"RoutingVersion"`
	Updated        int       `json:"updated"`
	ModifiedTime   time.Time `json:"ModifiedTime"`
	Copyright      string    `json:"copyright"`
	ModelVersion   int       `json:"ModelVersion"`
	Expires        time.Time `json:"Expires"`
	Contents       []struct {
		ModifiedTime time.Time `json:"ModifiedTime"`
		Language     string    `json:"Language"`
		IsModified   bool      `json:"IsModified"`
		ContentName  string    `json:"ContentName"`
		Expires      time.Time `json:"Expires"`
		ETag         string    `json:"ETag"`
		Data         struct {
			ContentName string `json:"ContentName"`
			Language    string `json:"Language"`
			Items       []struct {
				YWgs        float64 `json:"y_wgs"`
				Description string  `json:"Description"`
				Title       string  `json:"Title"`
				ContentName string  `json:"ContentName"`
				XWgs        float64 `json:"x_wgs"`
				CrsID       string  `json:"CrsId"`
				Sunki       float64 `json:"sunki"`
				Veter       float64 `json:"veter"`
				Y           float64 `json:"Y"`
				X           float64 `json:"X"`
				ID          string  `json:"Id"`
				Icon        string  `json:"Icon"`
			} `json:"Items"`
		} `json:"Data"`
	} `json:"Contents"`
}

type WindMeasurement struct {
	Id        string
	City      string
	WindSpeed float64
	WindGust  float64
	Lat       float64
	Lng       float64
	Time      int64
}

func Handler(ctx context.Context) error {
	m, err := parse()
	if err != nil {
		return fmt.Errorf("parsing failed: %v", err)
	}

	return store(ctx, m)
}

func parse() (measurements []WindMeasurement, err error) {
	client := &http.Client{Timeout: 10 * time.Second}

	r, err := client.Get("https://opendata.si/promet/burja/")
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()

	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	data := ResponseData{}

	err = json.Unmarshal(b, &data)
	if err != nil {
		return nil, err
	}

	for _, item := range data.Contents[0].Data.Items {
		measurements = append(measurements, WindMeasurement{
			Id:        item.ID + "-" + strconv.Itoa(data.Updated),
			City:      item.Title,
			WindSpeed: item.Veter,
			WindGust:  item.Sunki,
			Lat:       item.X,
			Lng:       item.Y,
			Time:      data.ModifiedTime.Unix(),
		})
	}

	return measurements, nil
}

func store(ctx context.Context, measurements []WindMeasurement) (err error) {
	awsConfig, err := external.LoadDefaultAWSConfig()

	if err != nil {
		return fmt.Errorf("oh, AWS config failed to load: %v", err)
	}

	db := dynamodb.New(awsConfig)

	tableName := os.Getenv("TABLE_NAME")

	for _, m := range measurements {
		dynamoAttribute, err := dynamodbattribute.MarshalMap(m)

		if err != nil {
			return fmt.Errorf("DynamoDB failed to marshal data, %v", err)
		}

		req := db.PutItemRequest(&dynamodb.PutItemInput{
			Item:      dynamoAttribute,
			TableName: aws.String(tableName),
		})

		_, err = req.Send(ctx)

		if err != nil {
			return fmt.Errorf("DynamoDB request failed: %v", err)
		}
	}

	return nil
}

func main() {
	lambda.Start(Handler)
}
