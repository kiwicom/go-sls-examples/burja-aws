# burja-aws

Simple Go application prepared for AWS Lambda.

## Structure

This simple application is consists of two main parts.

### Parser

Works as a cron, fetches the data from [opendata.si](https://opendata.si/promet/burja/) every hour and stores it to the [DynamoDB](https://aws.amazon.com/dynamodb/).

### API

Fetches the data from [DynamoDB](https://aws.amazon.com/dynamodb/), process it and returns it to the client.

## Setup

```bash
cp .env.example .env
```

Add your credentials (`AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`) to `.env` file. 

```bash
npm install
```

## Deploy

```bash
make deploy
```
