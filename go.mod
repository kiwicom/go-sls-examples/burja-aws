module gitlab.com/kiwicom/go-sls-examples/burja-aws

go 1.12

require (
	github.com/aws/aws-lambda-go v1.11.1
	github.com/aws/aws-sdk-go-v2 v0.9.0
)
